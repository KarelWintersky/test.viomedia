<?php

/**
 * User: Karel Wintersky
 * E-Mail: karel.wintersky@gmail.com
 *
 * Class UserConfig
 *
 */

class UserConfig
{
    const STORAGE_TABLE = 'userconfigs';

    const QUERY_GET = "SELECT storage FROM %s WHERE id = :id";
    const QUERY_SET = "REPLACE INTO %s (`id`, `storage`) VALUES (:id, :storage)";

    const DEFAULT_GLUE = '\\';

    const JSON_FLAGS = JSON_HEX_TAG || JSON_HEX_AMP || JSON_HEX_APOS || JSON_HEX_QUOT || JSON_NUMERIC_CHECK || JSON_PRESERVE_ZERO_FRACTION;

    private static $glue = null;

    /**
     * @var null
     */
    private static $storage = null;

    /**
     * @var null
     */
    private static $user_id = null;

    /**
     * self instance
     *
     * @var null
     */
    private static $_instance = null;

    /**
     * get Singletone instance
     *
     * @param $id
     * @param null $glue
     * @return null|UserConfig
     */
    public static function getInstance($id, $glue = null)
    {
        if (null === self::$_instance) {
            self::$_instance = new self($id, $glue);
        }

        return self::$_instance;
    }

    private function __clone() {}

    /**
     * UserConfig constructor.
     * @param $id
     * @param null $glue
     */
    private function __construct($id, $glue = null) {
        self::$glue = $glue ?? self::DEFAULT_GLUE;

        if (null == $id)
            return null;

        $query = self::getSQLQuery(self::QUERY_GET);

        $sth = DB::getConnection()->prepare($query);
        $sth->execute(['id' => $id]);

        $data = $sth->fetchColumn();

        if ($data) {
            self::$storage = self::unserialize($data);
        } else {
            self::$storage = [];
        }

        self::$user_id = $id;
    }

    /**
     * Getter
     *
     * @param $setting
     * @param null $default_value
     * @return array|mixed|null
     */
    public static function get($setting, $default_value = null)
    {
        if ($setting === '') {
            return $default_value;
        }

        if (!is_array($setting)) {
            $parents = explode(self::$glue, $setting);
        }

        $ref = &self::$storage;

        foreach ((array) $parents as $parent) {
            if (is_array($ref) && array_key_exists($parent, $ref)) {
                $ref = &$ref[$parent];
            } else {
                return $default_value;
            }
        }
        return $ref;
    }

    /**
     * Get All (debug)
     *
     * @return array|null
     */
    public static function getAll()
    {
        return self::$storage;
    }

    /**
     * Setter
     *
     * @param $setting
     * @param $value
     * @return array|bool
     */
    public static function set($setting, $value)
    {
        $parents = [];
        $setting = trim($setting);

        // can't set value for empty key
        if (empty($setting)) return false;

        // setting can't contain only glue
        if ($setting == self::$glue) return false;

        if (!is_array($setting)) {
            $parents = explode(self::$glue, (string)$setting);
        }

        if (empty($parents)) return false;

        $ref = &self::$storage;

        foreach ($parents as $parent) {
            if (isset($ref) && !is_array($ref)) {
                $ref = array();
            }

            $ref = &$ref[$parent];
        }

        $ref = $value;

        $data = [
            'storage'   =>  self::serialize(self::$storage),
            'id'        =>  self::$user_id,
        ];

        // update DB
        $sth = DB::getConnection()->prepare(self::getSQLQuery(self::QUERY_SET));
        $sth->execute($data);
        return $sth->errorInfo();
    }

    /**
     * @param $template
     * @return string
     */
    private static function getSQLQuery($template)
    {
        return sprintf($template, self::STORAGE_TABLE);
    }

    /**
     * @param $data
     * @return false|string
     */
    private static function serialize($data)
    {
        // return htmlspecialchars(serialize($data), ENT_QUOTES);

        return json_encode($data, self::JSON_FLAGS);
    }

    /**
     * @param $data
     * @return mixed
     */
    private static function unserialize($data)
    {
        // return unserialize(htmlspecialchars_decode($data));

        return json_decode($data, true, 512, self::JSON_FLAGS);
    }

}
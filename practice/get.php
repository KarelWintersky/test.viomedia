<?php
/**
 * User: Karel Wintersky
 * E-Mail: karel.wintersky@gmail.com
 *
 * Date: 25.09.2018, time: 19:35
 */
require_once 'DB.php';


$config = [
    'adapter'   =>  'mysql',
    'hostname'  =>  'localhost',
    'username'  =>  'phpauthdemo',
    'password'  =>  'password',
    'database'  =>  'phpauthdemo',
    'charset'   =>  'utf8',
    'port'      =>  3306
];

DB::init(NULL, $config);

require_once 'UserConfig.php';

$user = UserConfig::getInstance(2);

// $m = $user->get('username');

$m = $user->getAll();

var_dump($m);
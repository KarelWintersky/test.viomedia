<?php
/**
 * User: Karel Wintersky
 * E-Mail: karel.wintersky@gmail.com
 *
 * Date: 25.09.2018, time: 21:02
 */

require_once 'DB.php';

$config = [
    'adapter'   =>  'mysql',
    'hostname'  =>  'localhost',
    'username'  =>  'phpauthdemo',
    'password'  =>  'password',
    'database'  =>  'phpauthdemo',
    'charset'   =>  'utf8',
    'port'      =>  3306
];

DB::init(NULL, $config);

require_once 'UserConfig.php';

$user = UserConfig::getInstance(3);

$user->set('/', 'Somedata');

/*$user->set('username', 'Karel');

$user->set('home/location', 'St.Peterburg');

$user->set('work/new', ['role' => 'founder', 'address' => 'New-York']);

$user->set('username', 'Karel');

$user->set('quited', 'String with "quited" values ');
*/

